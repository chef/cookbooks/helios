package 'git'
package 'libpq-dev'
package 'postgresql-client'
package 'postgresql' if node['helios']['local_database']
package 'python3'
package 'python3-pip'
package 'python3-venv'
package 'rabbitmq-server'

dir = node['helios']['directory']

group 'helios' do
  system true
end

user 'helios' do
  comment 'Helios Voting'
  gid 'helios'
  home dir
  shell '/usr/sbin/nologin'
  system true
end

directory dir do
  owner 'helios'
  group 'helios'
  recursive true
end

execute 'Create PostgreSQL role for Helios' do
  command 'createuser helios'
  user 'postgres'
  only_if { node['helios']['local_database'] }
  not_if "psql -c '\\du' | grep -q '^\\s*helios\\s'"
end

execute 'Create PostgreSQL database for Helios' do
  command 'createdb helios'
  user 'postgres'
  only_if { node['helios']['local_database'] }
  not_if "psql -c '\\l' | grep -q '^\\s*helios\\s'"
end

execute 'Reset Helios database' do
  command "#{dir}/reset.sh"
  user 'postgres'
  group 'postgres'
  only_if { node['helios']['local_database'] }
  action :nothing
end

git dir do
  repository 'https://github.com/benadida/helios-server.git'
  user 'helios'
  group 'helios'
  revision node['helios']['revision']
  action :checkout
  notifies :run, 'execute[Reset Helios database]'
  not_if { ::Dir.exist? dir }
end

execute 'create virtual environment for Helios' do
  command "python3 -m venv '#{dir}/venv'"
  user 'helios'
  group 'helios'
  creates "#{dir}/venv"
end

package 'libldap2-dev'
package 'libsasl2-dev'

execute 'Install dependencies for Helios with pip' do
  command "#{dir}/venv/bin/pip install -r '#{dir}/requirements.txt'"
  cwd dir
  user 'helios'
  group 'helios'
end

template '/etc/default/helios' do
  source 'helios.erb'
  variables node['helios']
  sensitive true
  notifies :restart, 'systemd_unit[helios.service]'
  notifies :restart, 'systemd_unit[helios-worker.service]'
end

systemd_unit 'helios.service' do
  content <<-CONTENT.gsub(/^\s*/, '')
    [Unit]
    Description=Helios Election System
    Documentation=https://documentation.heliosvoting.org/

    [Service]
    User=helios
    Group=helios
    EnvironmentFile=/etc/default/helios
    ExecStart=#{dir}/venv/bin/python #{dir}/manage.py runserver

    [Install]
    WantedBy=multi-user.target
  CONTENT
  verify false
  action %i(create enable start)
end

systemd_unit 'helios-worker.service' do
  content <<-CONTENT.gsub(/^\s*/, '')
    [Unit]
    Description=Helios Election System Celery Worker
    Documentation=https://documentation.heliosvoting.org/

    [Service]
    User=helios
    Group=helios
    EnvironmentFile=/etc/default/helios
    ExecStart=#{dir}/venv/bin/celery worker --app helios --events --beat --concurrency 1
    WorkingDirectory=#{dir}

    [Install]
    WantedBy=multi-user.target
  CONTENT
  verify false
  action %i(create enable start)
end
