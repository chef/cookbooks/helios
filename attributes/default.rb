# General
default['helios']['celery_broker_url'] = 'amqp://localhost'
default['helios']['debug'] = true
default['helios']['directory'] = '/opt/helios'
default['helios']['master'] = false
default['helios']['revision'] = 'HEAD'

# Security
default['helios']['allowed_hosts'] = %w(localhost 127.0.0.1)
default['helios']['hsts'] = false
default['helios']['secret_key'] = 'replaceme'
default['helios']['secure_url_host'] = nil # same as 'url_host'
default['helios']['ssl'] = false
default['helios']['url_host'] = 'http://localhost:8000'

# Database
default['helios']['local_database'] = true
default['helios']['database_url'] = nil

# Email
default['helios']['admin_email'] = nil
default['helios']['admin_name'] = nil
default['helios']['default_from_email'] = 'ben@adida.net'
default['helios']['default_from_name'] = 'Ben for Helios'
default['helios']['email_host'] = 'localhost'
default['helios']['email_host_password'] = nil
default['helios']['email_host_user'] = nil
default['helios']['email_port'] = 2_525
default['helios']['email_use_aws'] = false
default['helios']['email_use_tls'] = false
default['helios']['help_email_address'] = 'help@heliosvoting.org'

# Appearance
default['helios']['allow_election_info_url'] = false
default['helios']['footer_links'] = '[]'
default['helios']['footer_logo_url'] = nil
default['helios']['main_logo_url'] = '/static/logo.png'
default['helios']['show_login_options'] = true
default['helios']['show_user_info'] = true
default['helios']['site_title'] = 'Helios Voting'
default['helios']['welcome_message'] = 'This is the default message'

# Authentication
default['helios']['auth_enabled_systems'] = 'password,google,facebook'
default['helios']['auth_default_system'] = nil

# Third-party tools
default['helios']['aws_access_key_id'] = nil
default['helios']['aws_secret_access_key'] = nil
default['helios']['cas_eligibility_realm'] = nil
default['helios']['cas_eligibility_url'] = nil
default['helios']['cas_password'] = nil
default['helios']['cas_username'] = nil
default['helios']['clever_client_id'] = nil
default['helios']['clever_client_secret'] = nil
default['helios']['facebook_api_key'] = nil
default['helios']['facebook_api_secret'] = nil
default['helios']['facebook_app_id'] = nil
default['helios']['gh_client_id'] = nil
default['helios']['gh_client_secret'] = nil
default['helios']['google_client_id'] = nil
default['helios']['google_client_secret'] = nil
default['helios']['mailgun_api_key'] = nil
default['helios']['rollbar_access_token'] = nil
